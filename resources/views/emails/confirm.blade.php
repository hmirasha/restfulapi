@component('mail::message')
# Hello {{$user->name}}

You cahnge your email, to verified your account, click on :

@component('mail::button', ['url' => route('verify',$user->verifiacation_token)])
Verified Account
@endcomponent

Thanks,<br>
{{ config('app.name') }}
@endcomponent
