@component('mail::message')
# Hello {{$user->name}}

To verified your account, click on :

@component('mail::button', ['url' => route('verify',$user->verifiacation_token)])
Verified Account
@endcomponent

Thanks,<br>
{{ config('app.name') }}
@endcomponent
