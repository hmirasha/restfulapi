<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
/*
Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});*/

/*
*
*Buyer
*
*/

Route::resource('buyers','Buyer\BuyerController')->only(['index','show']);
Route::resource('buyers.transactions','Buyer\BuyerTransactionController')->only(['index']);
Route::resource('buyers.products','Buyer\BuyerProductController')->only(['index']);
Route::resource('buyers.sellers','Buyer\BuyerSellerController')->only(['index']);
Route::resource('buyers.categories','Buyer\BuyerCategoryController')->only(['index']);
/*
*
*Category
*
*/
Route::resource('categories','Category\CategoryController')->except(['create','edit']);
Route::resource('categories.products','Category\CategoryProuctController')->only(['index']);
Route::resource('categories.sellers','Category\CategorySellerController')->only(['index']);
Route::resource('categories.transactions','Category\CategoryTransactionController')->only(['index']);
Route::resource('categories.buyers','Category\CategoryBuyernController')->only(['index']);
/*
*
*Product
*
*/
Route::resource('products','Product\ProductController')->only(['index','show']);
Route::resource('products.transactions','Product\ProductTransactionController')->only(['index']);
Route::resource('products.buyers','Product\ProductBuyerController')->only(['index']);
Route::resource('products.categories','Product\ProductCategoryController')->only(['index','update','destroy','show']);
Route::resource('products.buyers.transactions','Product\ProductBuyerTransactionController')->only(['store']);

/*
*
*Seller
*
*/
Route::resource('sellers','Seller\SellerController')->only(['index','show']);
Route::resource('sellers.transactions','Seller\SellerTransactionController')->only(['index']);
Route::resource('sellers.categories','Seller\SellerCategoryController')->only(['index']);
Route::resource('sellers.buyers','Seller\SellerBuyerController')->only(['index']);
Route::resource('sellers.products','Seller\SellerProductController')->only(['index','store','update','destroy','show']);
/*
*
*Transaction
*
*/
Route::resource('transactions','Transaction\TransactionController')->only(['index','show']);
Route::resource('transactions.categories','Transaction\TransactionCategoryController')->only(['index']);
Route::resource('transactions.seller','Transaction\TransactionSellerController')->only(['index']);
/*
*
*User
*
*/
Route::resource('users','User\UserController')->except(['create','edit']);
Route::get('users/verify/{token}','User\UserController@verify')->name('verify');
Route::get('users/{id}/resend','User\UserController@resend')->name('resend');
/*
*
*
*
*/
Route::post('oauth/token','\Laravel\Passport\Http\Controllers\AccessTokenController@issueToken');