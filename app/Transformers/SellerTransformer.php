<?php

namespace App\Transformers;

use League\Fractal\TransformerAbstract;
use App\Seller;

class SellerTransformer extends TransformerAbstract
{
    /**
     * List of resources to automatically include
     *
     * @var array
     */
    protected $defaultIncludes = [
        //
    ];
    
    /**
     * List of resources possible to include
     *
     * @var array
     */
    protected $availableIncludes = [
        //
    ];
    
    /**
     * A Fractal transformer.
     *
     * @return array
     */
    public function transform(Seller $seller)
    {
        return [
            'identifier'=>(int)$seller->id,
            'name'=>(string)$seller->name,
            'email'=>(string)$seller->email,
            'isVerified'=>(int) $seller->verified,
            'creationDate'=>(string)$seller->created_at,
            'lastChang'=>(string)$seller->updated_at,
            'deletedDate'=>isset($seller->deleted_at)?(string)$seller->deleted_at:null,
            'links'=>[
                [
                  'rel' =>'self',
                  'href'=>route('sellers.show',$seller->id),
                ],[
                  'rel' =>'sellers.categories',
                  'href'=>route('sellers.categories.index',$seller->id),
                ],[
                 'rel' =>'sellers.products',
                 'href'=>route('sellers.products.index',$seller->id),
                ],[
                  'rel' =>'sellers.buyers',
                  'href'=>route('sellers.buyers.index',$seller->id),
                ],[
                  'rel' =>'sellers.transactions',
                  'href'=>route('sellers.transactions.index',$seller->id),
                ],[
                   'rel' =>'user',
                   'href'=>route('users.show',$seller->id),
                ],
            ],
        ];
    }

    public static function originalAttribute($index)
    {
        $attrubutes= [
            'identifier'=>'id',
            'name'=>'name',
            'email'=>'email',
            'isVerified'=>'verified',
            'creationDate'=>'created_at',
            'lastChang'=>'updated_at',
            'deletedDate'=>'deleted_at',
        ];
        return isset($attrubutes[$index]) ? $attrubutes[$index]:null;
    }

    public static function transformedAttribute($index)
    {
        $attrubutes= [
            'id' =>'identifier',
            'name'=>'name',
            'email'=>'email',
            'verified' =>'isVerified',
            'created_at' =>'creationDate',
            'updated_at' =>'lastChang',
            'deleted_at' =>'deletedDate',
        ];
        return isset($attrubutes[$index]) ? $attrubutes[$index]:null;
    }
}
