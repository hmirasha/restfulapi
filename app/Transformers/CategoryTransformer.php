<?php

namespace App\Transformers;

use League\Fractal\TransformerAbstract;
use App\Category;

class CategoryTransformer extends TransformerAbstract
{
    /**
     * List of resources to automatically include
     *
     * @var array
     */
    protected $defaultIncludes = [
        //
    ];
    
    /**
     * List of resources possible to include
     *
     * @var array
     */
    protected $availableIncludes = [
        //
    ];
    
    /**
     * A Fractal transformer.
     *
     * @return array
     */
    public function transform(Category $category)
    {
        return [
            'identifier'=>(int)$category->id,
            'title'=>(string)$category->name,
            'details'=>(string)$category->description,
            'creationDate'=>(string)$category->created_at,
            'lastChang'=>(string)$category->updated_at,
            'deletedDate'=>isset($category->deleted_at)?(string)$category->deleted_at:null,
            
           'links'=>[
               'rel'=>'Self',
               'href'=>route('categories.show',$category->id),
           ],[
            'rel'=>'categories.products',
            'href'=>route('categories.products.index',$category->id),
            ],[
                'rel'=>'categories.sellers',
                'href'=>route('categories.sellers.index',$category->id),
            ],[
                'rel'=>'categories.transactions',
                'href'=>route('categories.transactions.index',$category->id),
            ],[
                'rel'=>'categories.buyers',
                'href'=>route('categories.buyers.index',$category->id),
            ],
        ];
    }

    public static function originalAttribute($index)
    {
        $attrubutes= [
            'identifier'=>'id',            
            'title'=>'name',
            'details'=>'description',
            'creationDate'=>'created_at',
            'lastChang'=>'updated_at',
            'deletedDate'=>'deleted_at',
        ];
        return isset($attrubutes[$index]) ? $attrubutes[$index]:null;
    }

    public static function transformedAttribute($index)
    {
        $attrubutes= [
            'id' =>'identifier',            
            'name' =>'title',
            'description' =>'details',
            'created_at' =>'creationDate',
            'updated_at' =>'lastChang',
            'deleted_at' =>'deletedDate',
        ];
        return isset($attrubutes[$index]) ? $attrubutes[$index]:null;
    }
}
