<?php

namespace App\Transformers;

use League\Fractal\TransformerAbstract;
use App\Transaction;

class TransactionTransformer extends TransformerAbstract
{
    /**
     * List of resources to automatically include
     *
     * @var array
     */
    protected $defaultIncludes = [
        //
    ];
    
    /**
     * List of resources possible to include
     *
     * @var array
     */
    protected $availableIncludes = [
        //
    ];
    
    /**
     * A Fractal transformer.
     *
     * @return array
     */
    public function transform(Transaction $transaction)
    {
        return [
            'identifier'=>(int)$transaction->id,
            'quantity'=>(int)$transaction->quantity,
            "product"=>(int)$transaction->product_id,
            "buyer"=>(int)$transaction->buyer_id,
            'creationDate'=>(string)$transaction->created_at,
            'lastChang'=>(string)$transaction->updated_at,
            'deletedDate'=>isset($transaction->deleted_at)?(string)$transaction->deleted_at:null,
            'links'=>[
                [
                  'rel' =>'self',
                  'href'=>route('transactions.show',$transaction->id),
                ],[
                  'rel' =>'transactions.product',
                  'href'=>route('products.show',$transaction->product_id),
                ],[
                  'rel' =>'transactions.buyer',
                  'href'=>route('buyers.show',$transaction->buyer_id),
                ],[
                  'rel' =>'transactions.categories',
                  'href'=>route('transactions.categories.index',$transaction->id),
                ],[
                  'rel' =>'transactions.seller',
                  'href'=>route('transactions.seller.index',$transaction->id),
                ],
            ],
        ];
    }
    public static function originalAttribute($index)
    {
        $attrubutes= [
            'identifier'=>'id',
            'quantity'=>'quantity',
            'product'=>'product_id',
            'buyer'=>'buyer_id',
            'creationDate'=>'created_at',
            'lastChang'=>'updated_at',
            'deletedDate'=>'deleted_at',
        ];
        return isset($attrubutes[$index]) ? $attrubutes[$index]:null;
    }

    public static function transformedAttribute($index)
    {
        $attrubutes= [
            'id' =>'identifier',
            'quantity'=>'quantity',
            'product_id' =>'product',
            'buyer_id' =>'buyer',
            'created_at' =>'creationDate',
            'updated_at'=>'lastChang',
            'deleted_at'=>'deletedDate',
        ];
        return isset($attrubutes[$index]) ? $attrubutes[$index]:null;
    }
}
