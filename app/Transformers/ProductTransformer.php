<?php

namespace App\Transformers;

use League\Fractal\TransformerAbstract;
use App\Product;

class ProductTransformer extends TransformerAbstract
{
    /**
     * List of resources to automatically include
     *
     * @var array
     */
    protected $defaultIncludes = [
        //
    ];
    
    /**
     * List of resources possible to include
     *
     * @var array
     */
    protected $availableIncludes = [
        //
    ];
    
    /**
     * A Fractal transformer.
     *
     * @return array
     */
    public function transform(Product $product)
    {
        return [
            'identifier'=>(int)$product->id,
            'title'=>(string)$product->name,
            'details'=>(string)$product->description,
            "stack"=>(int)$product->quantity,
            "sitatus"=>(string)$product->status,
            "picture"=>url("images/{$product->image}"),
            "seller"=>(int)$product->seller_id,
            'creationDate'=>(string)$product->created_at,
            'lastChang'=>(string)$product->updated_at,
            'deletedDate'=>isset($product->deleted_at)?(string)$product->deleted_at:null,

            'links'=>[
                [
                  'rel' =>'self',
                  'href'=>route('products.show',$product->id),
                ],[
                  'rel' =>'products.transactions',
                  'href'=>route('products.transactions.index',$product->id),
                ],[
                  'rel' =>'products.buyers',
                  'href'=>route('products.buyers.index',$product->id),
                ],[
                  'rel' =>'products.categories',
                  'href'=>route('products.categories.index',$product->id),
                ],[
                  'rel' =>'product.seller',
                  'href'=>route('sellers.show',$product->seller_id),
                ],
            ],
        ];
    }

    public static function originalAttribute($index)
    {
        $attrubutes= [
            'identifier'=>'id',            
            'title'=>'name',
            'details'=>'description',
            'stack'=>'quantity',
            'sitatus'=>'status',
            'picture'=>'image',
            'seller'=>'seller_id',
            'creationDate'=>'created_at',
            'lastChang'=>'updated_at',
            'deletedDate'=>'deleted_at',
        ];
        return isset($attrubutes[$index]) ? $attrubutes[$index]:null;
    }

    public static function transformedAttribute($index)
    {
        $attrubutes= [
            'id'=>'identifier',            
            'name'=>'title',
            'description' =>'details',
            'quantity' =>'stack',
            'status' =>'sitatus',
            'image' =>'picture',
            'seller_id' =>'seller',
            'created_at' =>'creationDate',
            'updated_at' =>'lastChang',
            'deleted_at' =>'deletedDate',
        ];
        return isset($attrubutes[$index]) ? $attrubutes[$index]:null;
    }
}
