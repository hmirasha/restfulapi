<?php

namespace App\Transformers;

use League\Fractal\TransformerAbstract;
use App\User;

class UserTransformer extends TransformerAbstract
{
    /**
     * List of resources to automatically include
     *
     * @var array
     */
    protected $defaultIncludes = [
        //
    ];
    
    /**
     * List of resources possible to include
     *
     * @var array
     */
    protected $availableIncludes = [
        //
    ];
    
    /**
     * A Fractal transformer.
     *
     * @return array
     */
    public function transform(User $user)
    {
        return [
            'identifier'=>(int)$user->id,
            'name'=>(string)$user->name,
            'email'=>(string)$user->email,
            'isVerified'=>(int) $user->verified,
            'isAdmin'=>($user->admin=='true'),
            'creationDate'=>(string)$user->created_at,
            'lastChang'=>(string)$user->updated_at,
            'deletedDate'=>isset($user->deleted_at)?(string)$user->deleted_at:null,
            'links'=>[
                [
                  'rel' =>'self',
                  'href'=>route('buyers.show',$user->id),
                ], 
            ],
        ];
    }

    public static function originalAttribute($index)
    {
        $attrubutes= [
            'identifier'=>'id',
            'name'=>'name',
            'email'=>'email',
            'isVerified'=>'verified',
            'isAdmin'=>'admin',
            'creationDate'=>'created_at',
            'lastChang'=>'updated_at',
            'deletedDate'=>'deleted_at',
        ];
        return isset($attrubutes[$index]) ? $attrubutes[$index]:null;
    }

    public static function transformedAttribute($index)
    {
        $attrubutes= [
            'id' =>'identifier',
            'name'=>'name',
            'email'=>'email',
            'verified' =>'isVerified',
            'admin' =>'isAdmin',
            'created_at' =>'creationDate',
            'updated_at' =>'lastChang',
            'deleted_at' =>'deletedDate',
        ];
        return isset($attrubutes[$index]) ? $attrubutes[$index]:null;
    }
}
