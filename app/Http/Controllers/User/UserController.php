<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\ApiController;
use App\User;
use Illuminate\Mail\Mailable;
use App\Mail\UserCreated;
use Illuminate\Http\Request;
use App\Transformers\UserTransformer;
use Illuminate\Support\Facades\Auth;

class UserController extends ApiController
{
    public function __construct()
    {
        $this->middleware('client')->only(['store','resend']);
        $this->middleware('auth:api')->except(['store','resend','verify']);
        $this->middleware('transform.input:'.UserTransformer::class)->only(['store','update']);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users =User::all();

        return $this->showAll($users);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules=[
            'name'=>'required',
            'password'=>'required|min:6|confirmed',
            'email'=>'required|email|unique:users',
        ];
        $this->validate($request,$rules);
        $data=$request->all();
        $data['password']=bcrypt($request->password);
        $data['verified']=User::UNVERIFIED_USER;
        $data['verifiacation_token']=User::getVerificationCode();
        $data['admin']=User::REGULAR_USER;
        $user=User::create($data);

        return $this->showOne($user,201);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function show(User $user)
    {
        //$user= User::findOrFail($user);
        return $this->showOne($user);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function edit(User $user)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, User $user)
    {
        $rules=[
            'password'=>'min:6|confirmed',
            'email'=>'email|unique:users,email,'.$user->id,
            'admin'=>'in:'.User::ADMIN_USER.','.User::REGULAR_USER,
        ];

        if($request->has('name')){
            $user->name=$request->name;
        }
        if($request->has('password')){
           $user->password=bcrypt($request->password); 
        }
        if($request->has('email')){
            $user->verified=User::UNVERIFIED_USER;
            $user->verifiacation_token=User::getVerificationCode();
            $user->email=$request->email;
        }
        if($request->has('admin')){
            if(!$user->isVerified()){
                return $this->errorResponse('Only verifed users can edit admin field',409);
            }
            $user->admin=$request->admin;
        }
        if(!$user->isDirty()){
            return $this->errorResponse('You need to specify a diffrent value',422);
        }
        $user->save();
        return $this->showOne($user);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $user)
    {
       // $user= User::findOrFail($user);
        $user->delete();
        return $this->showOne($user);
    }
    
    public function verify($token)
    {
        $user=User::where('verifiacation_token',$token)->firstOrFail();
        $user->verified=User::VERIFIED_USER;
        $user->verifiacation_token=null;
        $user->save();
        return $this->showMessage('The specified user has been verified');
    }

    public function resend(User $user)
    {
        if($user->isVerified()){
            return $this->errorResponse('This user is already verified',409);
        }
        retry(5,function($user){
            Mail::to($user)->send(new UserCreated($user));
        },1000);
        
        return $this->showMessage('The verified email has been sent');
    }
}