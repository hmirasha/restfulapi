<?php

namespace App\Http\Controllers\Product;

use App\Http\Controllers\ApiController;
use App\Product;
use App\User;
use App\Transaction;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Transformers\TransactionTransformer;

class ProductBuyerTransactionController extends ApiController
{
    public function __constructor()
    {
        parent::__construct();
        $this->middleware('transform.input:'.TransactionTransformer::class)->only(['store']);
    }
    
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request,Product $product, User $buyer)
    {
        $rules=[
            'quantity'=>'required',
        ];
        $this->validate($request,$rules);
        if($buyer->id== $product->seller_id){
            return $this->errorResponse('The buyer must be different from seller',409);
        
        }
        if(!$buyer->isVerified()){
            return $this->errorResponse('The buyer must be a verified user',409);
        }
        if(!$product->seller->isVerified()){
            return $this->errorResponse('The seller must be a verified user',409);
        }
        if($product->status==Product::UNAVAILABLE_PRODUCT){
            return $this->errorResponse('The product is not available',409);
        }
        if($product->quantity < $request->quantity){
            return $this->errorResponse('The product is not enough for this request',409);
        }
        return DB::transaction(function() use ($request,$product,$buyer)
        {
            $product->quantity -=$request->quantity;
            $product->save();
            $transaction=Transaction::create([
                'quantity'=>$request->quantity,
                'product_id'=>$product->id,
                'buyer_id'=>$buyer->id,
            ]);
            return $this->showOne($transaction,201);
        });
    }
}
