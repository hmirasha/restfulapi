<?php

namespace App\Providers;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\ServiceProvider;
use App\Product;
use App\User;
use Illuminate\Mail\Mailable;
use App\Mail\UserCreated;
use App\Mail\UserMailChanged;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *8
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Schema::defaultStringLength(191);

       /* User::created(function($user)
        {
           retry(5,function ($user){
             Mail::to($user)->send(new UserCreated ($user));
           },1000);     
        });*/

        User::updated(function($user)
        {
            if($user->isDirty('email')){
               retry(5,function($user)
               {
                  Mail::to($user)->send(new UserMailChanged($user));
               } ,1000);
            }
        });

        Product::updated(function ($product)
        {
            if($product->quantity==0 && $product->status==Product::AVAILABLE_PRODUCT){
                $product->status=Product::UNAVAILABLE_PRODUCT;
                $product->save();
            }
        });
    }
}
