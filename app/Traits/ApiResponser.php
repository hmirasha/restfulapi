<?php 
namespace App\Traits;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Validator;
/**
 * 
 */
trait ApiResponser
{
    private function successResponse($data,$code)
    {
        return response()->json($data,$code);
    }

    protected function errorResponse($message,$code)
    {
        return response()->json(['error'=>$message,'code'=>$code],$code);
    }

    public function showAll(Collection $collection,$code=200)
    {
        if($collection->isEmpty()){
            return $this->successResponse(['data'=>$collection],$code);    
        }
        $transformer=$collection->first()->transformer;
        $collection= $this->filterData($collection,$transformer);
        $collection= $this->sortData($collection,$transformer);
        $collection= $this->paginate($collection);
        $collection=$this->transformerData($collection,$transformer);
        $collection=$this->cachResponse($collection);
        return $this->successResponse($collection,$code);
    }

    public function showOne(Model $instance,$code=200)
    {
        $transformer=$instance->transformer;
        $instance=$this->transformerData($instance,$transformer);
        return $this->successResponse($instance,$code);
    }

    public function showMessage($message,$code=200)
    {
        return $this->successResponse(['data'=>$message],$code);
    }

    public function filterData(Collection $collection,$transformer)
    {
        foreach (request()->query() as $query => $value) {
            $attributes=$transformer::originalAttribute($query);
            if (isset($attributes,$value)) {
                $collection=$collection->where($attributes,$value);
            }
        }
        return $collection;
    }

    protected function sortData(Collection $collection,$transformer)
    {
        if(request()->has('sort_by')){
            $attribute= $transformer::originalAttribute(request()->sort_by);
            $collection= $collection->sortBy->{$attribute};
        }
        return $collection;
    }

    protected function paginate(Collection $collection)
    {
        $rules=[
            'per_page'=>'integer|min:1|max:50',
        ];
        Validator::validate(request()->all(),$rules);
        $page = LengthAwarePaginator::resolveCurrentPage();
        $perPage=15;
        if(request()->has('per_page')){
            $perPage =(int) request()->per_page;
        }
        $result=$collection->slice(($page-1)*$perPage,$perPage)->values();
        $paginated=new LengthAwarePaginator($result,$collection->count(),$perPage,$page,[
            'path'=>LengthAwarePaginator::resolveCurrentPage(),
            ]);
            $paginated->appends(request()->all());
            return $paginated;
    }

    protected function transformerData($data,$transformer)
    {
        $transformtion=fractal($data,new $transformer);
        return $transformtion->toArray();
    }

    protected function cachResponse($data)
    {
        $url =request()->url();

        $queryParameter= request()->query();

        ksort($queryParameter);

        $queryString=http_build_query($queryParameter);

        $fullUrl= "{$url}?{$queryString}";

        return Cache::remember($fullUrl,30/60,function () use ($data)
        {
            return $data;
        });
    }
}
