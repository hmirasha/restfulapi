<?php

namespace App\Exceptions;
use App\Traits\ApiResponser;
use Exception;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Illuminate\Validation\ValidationException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Support\Str;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Auth\AuthenticationException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpKernel\Exception\MethodNotAllowedHttpException;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Illuminate\Database\QueryException;

class Handler extends ExceptionHandler
{
    use ApiResponser;
    /**
     * A list of the exception types that are not reported.
     *
     * @var array
     */
    protected $dontReport = [
        //
    ];

    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var array
     */
    protected $dontFlash = [
        'password',
        'password_confirmation',
    ];

    /**
     * Report or log an exception.
     *
     * @param  \Exception  $exception
     * @return void
     */
    public function report(Exception $exception)

    {
        parent::report($exception);
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Exception  $exception
     * @return \Illuminate\Http\Response
     */
    public function render($request, Exception $exception)
    {
        if ($exception instanceof ValidationException) {
            return $this->convertValidationExceptionToResponse($exception,$request);
        }

        if ($exception instanceof ModelNotFoundException) {
            $modelName=Str::lower(class_basename($exception->getModel()));
            return $this->errorResponse("No {$modelName} found",404);
        }

      /*  if ($exception instanceof AuthenticationException) {
        /*    if($this->isFrontEnd($request)){
                return redirect()->guest('login');
            }
            return $this->errorResponse('Unauthentication',401);
        }*/

        if ($exception instanceof AuthorizationException) {
            return $this->errorResponse($exception->getMessage(),403);
        }

        if ($exception instanceof NotFoundHttpException) {
            return $this->errorResponse("Not found http url",404);
        }

        if ($exception instanceof MethodNotAllowedHttpException) {
            return $this->errorResponse("The specific method for request is invalid",405);
        }
        
        if ($exception instanceof HttpException) {
            return $this->errorResponse($exception->getMessage(),$exception->getStatusCode());
        }
        
        if ($exception instanceof QueryException) {
            $errorCode=$exception->errorInfo[1];
            if($errorCode==1451){
              return $this->errorResponse("Cann't remove this resource",409);  
            }
        }

        if(config('app.debug')){
            return parent::render($request, $exception);
        }
        
        return $this->errorResponse("Unexpected Errors, Try Later ...",500); 
    }
    

    public function convertValidationExceptionToResponse(ValidationException $e,$request)
    {
        $errors=$e->validator->errors()->getMessages();

      /*  if($this->isFrontEnd($request)){
            return $request->ajax()?response()->json($errors,422):redirect()->back()->withInput($request->input())->with($request->errers);
        }*/
        return $this->errorResponse($errors,422);
    }

 /*   private function isFrontEnd($request)
    {
        return $request->acceptsHtml() && collect($request->route()->middleware())->contains('web');
    }*/
}