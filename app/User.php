<?php

namespace App;

use Laravel\Passport\HasApiTokens;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Str;
use App\Transformers\UserTransformer;
use Illuminate\Database\Eloquent\SoftDeletes;


class User extends Authenticatable
{
    use Notifiable, SoftDeletes;
    use HasApiTokens;
    const ADMIN_USER="true";
    const REGULAR_USER="false";

    const VERIFIED_USER="1";
    const UNVERIFIED_USER="0";
    
    protected $table='users';
    public $transformer=UserTransformer::class;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'email',
        'password',
        'verified',
        'verifiacation_token',
        'admin',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
        //'verifiacation_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];
/*
    public function setNameAttribute($name)
    {
        $this->attributes['name']=Str::lower($name);
    }
    
    public function getNameAttribute()
    {
        return Str::camel($this->name);
    }

    public function setEmailAttribute($email)
    {
        $this->attributes['email']=Str::lower($email);
    }
*/
    public function isAdmin()
    {
        return $this->admin==User::ADMIN_USER;
    }

    public function isVerified()
    {
        return $this->verified==User::VERIFIED_USER;
    }

    public static function getVerificationCode()
    {
        return Str::random(40);
    }
}
