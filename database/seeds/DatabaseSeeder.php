<?php
use App\User;
use App\Category;
use App\Product;
use App\Transaction;
use App\Seller;
use App\Buyer;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);
        DB::statement('SET FOREIGN_KEY_CHECKS = 0');
        User::truncate();
        Category::truncate();
        Product::truncate();
        Transaction::truncate();
        DB::table('category_product')->truncate();
        
        $usersCount=1000;
        $CategoriesCount=500;
        $ProductsCount=600;
        $transactionsCount=800;

        factory(User::class,$usersCount)->create();
        factory(Category::class,$CategoriesCount)->create();
        factory(Product::class,$ProductsCount)->create()->each(
            function($product){
                $categories=Category::all()->random(mt_rand(1,5))->pluck('id');
                $product->categories()->attach($categories);
            });
        factory(Transaction::class,$transactionsCount)->create();

    }
}
